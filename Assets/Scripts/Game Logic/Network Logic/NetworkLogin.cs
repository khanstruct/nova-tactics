﻿using UnityEngine;
using System.Collections;

public sealed class NetworkLogin : MonoBehaviour {
    private static NetworkLogin _instance = null; // Cache reference
    private static readonly string _tag = "Network Login";

    public static NetworkLogin Instance {
        get {
            if (NetworkLogin._instance == null) {
                GameObject networkLogin = GameObject.FindGameObjectWithTag(NetworkLogin._tag);

                if (networkLogin != null) {
                    NetworkLogin._instance = networkLogin.GetComponent<NetworkLogin>();
                }

                if (NetworkLogin._instance == null) {
                    DebugOutput.DisplayError(typeof(NetworkLogin).ToString(), "Instance", "Could not find component with tag '" + NetworkLogin._tag + "'");
                }
            }

            return NetworkLogin._instance;
        }
    }

    public string username;
    public string password; // Will not be stored.
    private bool loggedIn;

    void Awake() {
        DontDestroyOnLoad(this.gameObject);
    }

    public void SetUsername(string username) {
        this.username = username;
    }

    public void SetPassword(string password) {
        this.password = password;

        Application.LoadLevel("Main Screen");
    }

    public bool IsLoggedIn {
        get {
            return this.loggedIn;
        }
        set {
            this.loggedIn = value;
        }
    }
}
