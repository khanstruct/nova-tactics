﻿using UnityEngine;
using System.Collections;

public class NetworkManager : MonoBehaviour {
	// Cached instance of NetworkManager
	private static NetworkManager _networkManager;
	
	public static NetworkManager NetworkInstance {
		get {
			if(_networkManager == null) {
				GameObject networkManager = new GameObject();
				networkManager.name = "$Network Manager$";
				networkManager.AddComponent("NetworkManager");
				DontDestroyOnLoad(networkManager);
				NetworkManager._networkManager = networkManager.GetComponent<NetworkManager>();
			}
			
			return NetworkManager._networkManager;
		}
	}
	
	private string regGameName = "Nova-Tactics-Master-Sever";
	private bool isRefreshing = false;
	private float refreshRequestLength = 3.0f;
	private HostData[] hostData;
	
	public void OnGUI() {
		if(Network.isClient) {
            GUI.Label(new Rect(50, 200, 200, 50), "Joined: " + this.regGameName);
			return;
		}

        if (Network.isServer) {
            GUI.Label(new Rect(50, 200, 200, 50), "Hosting: " + this.regGameName);
            return;
        }
		
		if(GUI.Button( new Rect(25.0f, 25.0f, 150.0f, 50.0f), "Start Server")) {
			this.StartServer(NetworkLogin.Instance.username + " Lobby");
		}
		if(GUI.Button(new Rect(25.0f, 100.0f, 150.0f, 50.0f), "Refresh Server List")) {
			StartCoroutine("RefreshHostList");
		}
		
		if(this.hostData != null) {
			for(int i = 0; i < this.hostData.Length; i++) {
				if(GUI.Button(new Rect(Screen.width/2, 65.0f * (50.0f * i), 150.0f, 50.0f), this.hostData[i].gameName)) {
					Debug.Log("Attempting to Connect...");
					Network.Connect(this.hostData[i]);
					Debug.Log(Network.TestConnection());
					Debug.Log("Connected? " + Network.isClient);
				}
			}
		}
	}
	
	// Function to start the server.
	private void StartServer(string lobbyName, int connections = 16, int listenPort = 25002, bool useNat = false) {
		Network.InitializeServer(connections, listenPort, useNat);
		MasterServer.RegisterHost(this.regGameName, lobbyName);
	}
	
	// Function to initialize the server.
	private void OnServerInitialized() {
		Debug.Log("Server " + regGameName + " has been initialized");
	}
	
	// Function for MasterServerEvent.
	private void OnMasterServerEvent(MasterServerEvent mSEvent) {
		if(mSEvent == MasterServerEvent.RegistrationSucceeded) {
			Debug.Log("Registration Success!");
		}
	}
	
	// Function for refresh list.
	private IEnumerator RefreshHostList() {
        if (this.isRefreshing == true) yield break;

		Debug.Log("Refreshing...");
        this.isRefreshing = true;
		MasterServer.RequestHostList(this.regGameName);
		
		float timeStarted = Time.time;
		float timeEnd = Time.time + this.refreshRequestLength;
		
		while(Time.time < timeEnd) {
			this.hostData = MasterServer.PollHostList();
			yield return new WaitForEndOfFrame();
		}
		
		if(this.hostData == null || this.hostData.Length == 0) {
			Debug.Log("No active servers found!");
		} else {
			Debug.Log(this.hostData.Length + " server has been found!");
		}

        this.isRefreshing = false;
	}
}
