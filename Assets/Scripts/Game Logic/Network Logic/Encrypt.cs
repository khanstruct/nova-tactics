﻿using UnityEngine;
using System;
using System.Collections;
using System.Security.Cryptography;
using System.Text;

public class Encrypt : MonoBehaviour {
    private static readonly string _salt = "lambda-spacetime";
    private static readonly SHA1 sha1 = new SHA1CryptoServiceProvider(); 

    /*public static string EncryptString(string input) {
        byte[] results = sha1.ComputeHash(GetBytes(input + _salt));

        return GetString(results);
    }

    static byte[] GetBytes(string str) {
        byte[] bytes = new byte[str.Length * sizeof(char)];

        System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);

        return bytes;
    }

    static string GetString(byte[] bytes) {
        char[] chars = new char[bytes.Length / sizeof(char)];

        System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);

        return new string(chars);
    }*/
    
    public static string ComputeSHA1(string strToEncrypt) {
        UTF8Encoding ue = new UTF8Encoding();
        byte[] bytes = ue.GetBytes(strToEncrypt);

        byte[] hashBytes = sha1.ComputeHash(bytes);

        string hashString = "";
 
        for (int i = 0; i < hashBytes.Length; i++)
        {
            hashString += Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
        }
 
        return hashString.PadLeft(32, '0');
    }
}
