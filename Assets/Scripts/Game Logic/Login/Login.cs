﻿using UnityEngine;
using System.Collections;

public class Login : MonoBehaviour {
    public UIInput username;
    public UIInput password;

    private string _password;

    public void Submit() {

        if (this.IsValidInput(this.username.value) == false) {
            this.username.value = "";
            this.username.label.text = "Empty!";

            return;
        }

        if (this.IsValidInput(this.password.value) == false) {
            this.password.value = "";
            this.password.label.text = "Empty!";

            return;
        }

        this._password = this.password.value;
        this.password.value = "";

        NetworkLogin.Instance.SetUsername(this.username.value);
        NetworkLogin.Instance.SetPassword(Encrypt.ComputeSHA1(this._password));
    }

    public bool IsValidInput(string input) {
        if (string.IsNullOrEmpty(input) == true) {
            return false;
        }
        else {
            return true;
        }
    }
}
