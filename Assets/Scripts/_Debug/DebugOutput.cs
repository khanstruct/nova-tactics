﻿using UnityEngine;
using System.Collections;

public class DebugOutput : MonoBehaviour {
    public bool showDebug;

    public static void DisplayNormal(string fromClass, string method, string message) {
        Debug.Log(fromClass + " - " + method + ": " + message);
    }

    public static void DisplayWarning(string fromClass, string method, string message) {
        Debug.LogWarning(fromClass + " - " + method + ": " + message);
    }

    public static void DisplayError(string fromClass, string method, string message) {
        Debug.LogError(fromClass + " - " + method + ": " + message);
    }
}
